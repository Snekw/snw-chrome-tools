# Snw-chrome-tools

This is a collection of tools that I think are useful to have available to developers with ease.

## Features

* Remove element
* Rotate element
* View formatted JSON


License: GPL-3.0

https://gitlab.com/Snekw/snw-chrome-tools

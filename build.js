/**
 *  snw-chrome-tools,
 *  Copyright (C) 2018 Ilkka Kuosmanen
 *
 *  This file is part of snw-chrome-tools.
 *
 *  snw-chrome-tools is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  snw-chrome-tools is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with snw-chrome-tools.  If not, see <http://www.gnu.org/licenses/>.
 */
'use strict';
const rollup = require('rollup').rollup;
const multiEntry = require('rollup-plugin-multi-entry');
const nodeResolve = require('rollup-plugin-node-resolve');
const commonjs = require('rollup-plugin-commonjs');
const fs = require('fs');
const path = require('path');
const parser = require('yargs-parser');

const paramOpts = {
  alias: {dev: ['d', 'development']},
  boolean: ['dev']
};

const params = parser(process.argv.slice(2));

const isDev = params.dev;

function getDest () {
  return isDev ? 'devBuild' : 'dist';
}

async function makeBundleFromDir (dir, name, destDir) {
  const bundle = await rollup({
    input: `${dir}/**/*.js`,
    plugins: [multiEntry(), nodeResolve(), commonjs()]
  });

  await bundle.write({
    file: `${getDest()}${destDir ? '/' + destDir : ''}/${name}.js`,
    format: 'iife',
    name: 'snwChromeTools'
  });
}

async function alterManifest () {
  const manifest = JSON.parse(fs.readFileSync('src/manifest.json').toString());
  manifest.name = 'Snekw\'s chrome tools';
  manifest.background.scripts = ['background.js'];
  manifest.content_scripts[0].js = ['content.js'];

  fs.writeFileSync(`${getDest()}/manifest.json`, JSON.stringify(manifest));
}

async function concatCss () {
  let cssOut = '';
  let cssFiles = ['mouseScript.css'];

  for (let file of cssFiles) {
    cssOut = fs.readFileSync(`src/content/${file}`);
  }

  fs.writeFileSync(`${getDest()}/main.css`, cssOut);
}

async function postProcess () {
  const files = ['background.js', 'content.js'];
  for (let file of files) {
    const fp = `${getDest()}/${file}`;
    const content = fs.readFileSync(fp).toString();
    let r = isDev ? /£€|€£/g : /£€.+€£/g;
    fs.writeFileSync(fp, content.replace(r, ''));
  }
}

async function copyFolder (folder, target) {
  fs.readdir(folder, (err, files) => {
    if (err) {
      throw err;
    }
    try {
      fs.mkdirSync(path.join(getDest(), target));
    } catch (e) { }
    files.forEach(file => fs.lstatSync(path.resolve(folder, file)).isFile() ? fs.copyFileSync(path.join(folder, file), path.join(getDest(), target, file)) : void 0);
  });
}

async function copyNodeModuleDep (depFile, target, name) {
  try {
    fs.mkdirSync(path.join(getDest(), target));
  } catch (e) { }
  fs.copyFileSync(path.join('node_modules', depFile), path.join(getDest(), target, name));
}

Promise.all([
  makeBundleFromDir('src/content', 'content'),
  makeBundleFromDir('src/background', 'background'),
  makeBundleFromDir('src/jsonFormatter/bundle', 'jsonFormatter', 'jsonFormatter'),
  alterManifest(),
  copyFolder('src/jsonFormatter', 'jsonFormatter')
  // copyNodeModuleDep('json-formatter-js/dist/json-formatter.js', 'jsonFormatter', 'json-formatter.js')
  // concatCss()
])
  .then(() => postProcess())
  .then(() => console.log('done'))
  .catch(err => console.error(err));

/**
 *  snw-chrome-tools,
 *  Copyright (C) 2018 Ilkka Kuosmanen
 *
 *  This file is part of snw-chrome-tools.
 *
 *  snw-chrome-tools is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  snw-chrome-tools is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with snw-chrome-tools.  If not, see <http://www.gnu.org/licenses/>.
 */
'use strict';
import JSONFormatter from 'json-formatter-js';

chrome.runtime.onMessage.addListener(function (message) {
  if (message.command !== 'json-print')
    return;
  let jsonData = message.data;
  if (typeof message.data !== 'object') {
    try {
      jsonData = JSON.parse(message.data);
    } catch (e) {
      document.write(e);
    }
  }
  const formatter = new JSONFormatter(jsonData, 1, {
    animateOpen: false,
    animateClose: false
  });
  document.body.appendChild(formatter.render());
});

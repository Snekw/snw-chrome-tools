/**
 *  snw-chrome-tools,
 *  Copyright (C) 2018 Ilkka Kuosmanen
 *
 *  This file is part of snw-chrome-tools.
 *
 *  snw-chrome-tools is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  snw-chrome-tools is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with snw-chrome-tools.  If not, see <http://www.gnu.org/licenses/>.
 */
'use strict';

let jsonFormatterTabId = 0;

export function jsonFormat (data) {
  console.log(data);

  chrome.tabs.get(jsonFormatterTabId, () => {
    if (chrome.runtime.lastError) {
      chrome.tabs.create({
        active: true,
        url: chrome.extension.getURL('jsonFormatter/jsonFormatter.html')
      }, (tab) => {
        jsonFormatterTabId = tab.id;
        chrome.tabs.sendMessage(tab.id, {command: 'json-print', data});
      });
    } else {
      chrome.tabs.sendMessage(jsonFormatterTabId, {command: 'json-print', data});
    }
  });
}

/**
 *  snw-chrome-tools,
 *  Copyright (C) 2018 Ilkka Kuosmanen
 *
 *  This file is part of snw-chrome-tools.
 *
 *  snw-chrome-tools is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  snw-chrome-tools is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with snw-chrome-tools.  If not, see <http://www.gnu.org/licenses/>.
 */

'use strict';

import { jsonFormat } from './commands';

function runRemoveElement () {
  chrome.tabs.executeScript({code: 'new snwChromeTools.SnwRemoveElement()'});
}

function runRotateElement () {
  chrome.tabs.executeScript({code: 'new snwChromeTools.SnwRotateElement()'});
}

function runJsonFormat (selection) {
  chrome.tabs.executeScript({code: `new snwChromeTools.SnwJsonFormat(${selection})`});
}

chrome.contextMenus.onClicked.addListener(function (info, tab) {
  console.log(info);
  console.log(tab);
  switch (info.menuItemId) {
    case 'removeElement':
      runRemoveElement();
      break;
    case 'rotateElement':
      runRotateElement();
      break;
    case 'jsonFormatter':
      runJsonFormat(false);
      break;
    case 'jsonFormattedSelection':
      runJsonFormat(info.selectionText);
      break;
  }
});

chrome.runtime.onInstalled.addListener(function () {
  chrome.contextMenus.create({
    title: 'Snw£€ (DEV)€£',
    contexts: ['page', 'frame', 'link', 'selection', 'editable', 'image', 'video', 'audio'],
    id: 'top'
  });
  chrome.contextMenus.create({
    title: 'Remove element',
    contexts: ['page', 'frame', 'link', 'selection', 'editable', 'image', 'video', 'audio'],
    id: 'removeElement',
    parentId: 'top'
  });
  chrome.contextMenus.create({
    title: 'Rotate element',
    contexts: ['page', 'frame', 'link', 'selection', 'editable', 'image', 'video', 'audio'],
    id: 'rotateElement',
    parentId: 'top'
  });
  chrome.contextMenus.create({
    title: 'View JSON formatted',
    contexts: ['page', 'frame', 'link', 'editable'],
    id: 'jsonFormatter',
    parentId: 'top'
  });
  chrome.contextMenus.create({
    title: 'View JSON formatted',
    contexts: ['selection'],
    id: 'jsonFormattedSelection',
    parentId: 'top'
  });
});

chrome.commands.onCommand.addListener(function (command) {
  switch (command) {
    case 'remove-element':
      runRemoveElement();
      break;
    case 'rotate-element':
      runRotateElement();
      break;
    default:
      break;
  }
});

// Messages
chrome.runtime.onMessage.addListener(function (request, sender, resp) {
  let reply;
  let replyData;
  switch (request.command) {
    case 'json':
      jsonFormat(request.data);
      break;
  }

  if (reply) {
    resp(replyData);
  }
});

/**
 *  snw-chrome-tools,
 *  Copyright (C) 2018 Ilkka Kuosmanen
 *
 *  This file is part of snw-chrome-tools.
 *
 *  snw-chrome-tools is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  snw-chrome-tools is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with snw-chrome-tools.  If not, see <http://www.gnu.org/licenses/>.
 */
'use strict';

export class Event {
  constructor (event, listener) {
    if (typeof event !== 'string') {
      throw `Invalid event type!`;
    }
    if (typeof listener !== 'function') {
      throw `Invalid listener type.`;
    }

    this.type = event;
    this.listener = listener;
  }

  emit (args) {
    this.listener(args);
  }
}

export class Eventer {
  constructor (allowedEvents) {
    this.eventTypes = Array.isArray(allowedEvents) ? allowedEvents : [];
    this.events = [];
  }

  on (event, listener) {
    if (this.eventTypes.includes(event)) {
      this.events.push(new Event(event, listener));
    } else {
      throw `Can't subscribe to event listener: "${event}" is not a valid event name!`;
    }
  }

  off (event, listener) {
    if (this.eventTypes.includes(event)) {
      this.events = this.events.filter(val => val.listener !== listener);
    } else {
      throw `Can't unsubscribe from event: "${event}" is not a valid event name!`;
    }
  }

  emit (event, args) {
    if (this.eventTypes.includes(event)) {
      this.events.filter(e => e.type === event).forEach(e => e.emit(args));
    } else {
      throw `Can't emit event: "${event}" is not a valid event name!`;
    }
  }

}

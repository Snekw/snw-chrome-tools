/**
 *  snw-chrome-tools,
 *  Copyright (C) 2018 Ilkka Kuosmanen
 *
 *  This file is part of snw-chrome-tools.
 *
 *  snw-chrome-tools is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  snw-chrome-tools is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with snw-chrome-tools.  If not, see <http://www.gnu.org/licenses/>.
 */
'use strict';

export class Highlighter {
  constructor () {
    this.destroyed = false;

    this.lastHighlighted = undefined;

    this.highlight = this.highlight.bind(this);
    this.destroy = this.destroy.bind(this);
    this.scroll = this.scroll.bind(this);

    this.shadowContainer = document.createElement('div');
    this.shadowStyle = document.createElement('style');
    this.shadowStyle.textContent = `
    *{
    margin:0;
    padding:0;
    cursor: crosshair;
    }
    
    svg{
    position: fixed;
    top: 0;
    left: 0;
    z-index: 2147483647;
    pointer-events:none;
    }
    `;

    this.shadow = this.shadowContainer.attachShadow({mode: 'closed'});

    this.highlighter = document.createElementNS('http://www.w3.org/2000/svg', 'svg');
    this.highlighterRect = document.createElementNS('http://www.w3.org/2000/svg', 'rect');
    this.highlighterRect.setAttribute('fill', '#c60015');
    this.highlighterRect.setAttribute('fill-opacity', '0.5');
    this.highlighter.setAttribute('id', 'snw-chrome-highlighter');
    this.highlighter.appendChild(this.highlighterRect);
    this.highlighter.setAttribute('xmlns', 'http://www.w3.org/2000/svg');
    this.highlighter.setAttribute('width', document.documentElement.clientWidth.toString());
    this.highlighter.setAttribute('height', window.innerHeight.toString());
    this.oldCursor = document.body.style.cursor;
    document.body.style.cursor = 'crosshair';
    this.shadow.appendChild(this.shadowStyle);
    this.shadow.appendChild(this.highlighter);
    document.body.appendChild(this.shadowContainer);
    window.addEventListener('scroll', this.scroll, {passive: true});
  }

  destroy () {
    if (!this.destroyed) {
      document.body.style.cursor = this.oldCursor;
      document.body.removeChild(this.shadowContainer);
      window.removeEventListener('scroll', this.scroll);
      this.destroyed = true;
    }
  }

  highlight (element) {
    this.lastHighlighted = element;
    const {x, y, width, height} = element.getBoundingClientRect();
    this.highlighterRect.setAttribute('x', x);
    this.highlighterRect.setAttribute('y', y);
    this.highlighterRect.setAttribute('width', width);
    this.highlighterRect.setAttribute('height', height);
  }

  setSize (width = undefined, height = undefined) {
    if (width === undefined) {
      width = document.documentElement.clientWidth;
    }
    if (height === undefined) {
      height = document.documentElement.scrollHeight;
    }
    this.highlighter.setAttribute('width', width.toString());
    this.highlighter.setAttribute('height', height.toString());
  }

  scroll () {
    this.highlight(this.lastHighlighted);
  }
}

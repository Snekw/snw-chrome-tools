/**
 *  snw-chrome-tools,
 *  Copyright (C) 2018 Ilkka Kuosmanen
 *
 *  This file is part of snw-chrome-tools.
 *
 *  snw-chrome-tools is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  snw-chrome-tools is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with snw-chrome-tools.  If not, see <http://www.gnu.org/licenses/>.
 */
'use strict';
import { Highlighter } from './highlighter';
import { ElementSelector } from './elementSelector';

export class SnwRotateElement {
  constructor () {
    this.destroyed = false;

    this.rotateElement = this.rotateElement.bind(this);
    this.destroy = this.destroy.bind(this);

    this.elementSelector = new ElementSelector();
    this.elementHighlighter = new Highlighter();
    this.elementHighlighter.setSize();
    this.elementSelector.on('selectedChanged', this.elementHighlighter.highlight);
    this.elementSelector.on('exit', this.destroy);
    this.elementSelector.on('selected', this.rotateElement);
  }

  destroy () {
    if (!this.destroyed) {
      this.elementHighlighter.destroy();
      this.elementHighlighter = undefined;
      this.elementSelector.destroy();
      this.elementSelector = undefined;
      this.destroyed = true;
    }
  }

  rotateElement (target) {
    let curRot = target.style.transform;
    let match = /rotate\( ?(\d+)deg/g.exec(curRot);

    if (match && match.length === 2) {
      curRot = parseInt(match[1]) + 90;
    } else {
      curRot = 90;
    }
    target.style.transform = `rotate(${curRot}deg)`;
    this.elementHighlighter.highlight(target);
  }
}

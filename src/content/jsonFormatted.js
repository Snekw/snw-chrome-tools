/**
 *  snw-chrome-tools,
 *  Copyright (C) 2018 Ilkka Kuosmanen
 *
 *  This file is part of snw-chrome-tools.
 *
 *  snw-chrome-tools is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  snw-chrome-tools is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with snw-chrome-tools.  If not, see <http://www.gnu.org/licenses/>.
 */
'use strict';

export class SnwJsonFormat {
  constructor (data) {
    this.data = data ? data : SnwJsonFormat.getData();
    this.openJsonPage();
  }

  static getData () {
    let pre = document.body.querySelector('pre');
    if (pre) {
      return pre.textContent;
    }
    pre = document.body.innerText;

    return pre;

  }

  openJsonPage () {
    chrome.runtime.sendMessage({command: 'json', data: this.data});
  }
}

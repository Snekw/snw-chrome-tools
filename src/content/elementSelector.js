/**
 *  snw-chrome-tools,
 *  Copyright (C) 2018 Ilkka Kuosmanen
 *
 *  This file is part of snw-chrome-tools.
 *
 *  snw-chrome-tools is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  snw-chrome-tools is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with snw-chrome-tools.  If not, see <http://www.gnu.org/licenses/>.
 */

'use strict';

import { Eventer } from './eventer';

export class ElementSelector extends Eventer {
  constructor () {
    super(['selectedChanged', 'selected', 'exit']);
    this.destroyed = false;

    this.selectedElement = undefined;

    this.mouseMove = this.mouseMove.bind(this);
    this.mouseClick = this.mouseClick.bind(this);
    this.keyUp = this.keyUp.bind(this);
    this.exit = this.exit.bind(this);
    this.destroy = this.destroy.bind(this);

    window.addEventListener('mousemove', this.mouseMove, {passive: true});
    document.body.addEventListener('click', this.mouseClick, true);
    document.body.addEventListener('keyup', this.keyUp, {passive: true, capture: true});
  }

  keyUp (e) {
    if (e.key === 'Escape') {
      this.exit();
    }
  }

  mouseMove (e) {
    if (e.target !== this.selectedElement) {
      this.selectedElement = e.target;
      this.emit('selectedChanged', e.target);
    }
  }

  mouseClick (e) {
    e.stopImmediatePropagation();
    e.stopPropagation();
    e.preventDefault();
    if (e.button === 0) {
      this.emit('selected', this.selectedElement);
    } else {
      this.exit();
    }
    return false;
  }

  exit () {
    this.destroy();
    this.emit('exit');
  }

  destroy () {
    if (!this.destroyed) {
      this.destroyed = true;
      window.removeEventListener('mousemove', this.mouseMove);
      document.body.removeEventListener('click', this.mouseClick, true);
      document.body.removeEventListener('keyup', this.keyUp);
    }
  }
}
